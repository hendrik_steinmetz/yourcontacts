package de.steinmetz.contacts.utils;

import java.io.BufferedWriter;

import javax.swing.JFrame;

public class Frame extends JFrame {
	
	/* de.Steinmetz.devUtils.Frame Version 1.0.0 */
	
	BufferedWriter write;
	
	public Frame(int width, int height, String title) {
		super(title);
		this.setSize(width, height);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setVisible(true);
	}
	
}
