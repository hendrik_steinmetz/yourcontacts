package de.steinmetz.contacts.core.gui;

import de.steinmetz.contacts.core.contact;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

public class editFrame extends JFrame implements ActionListener {
    private JTextField[] lbls = new JTextField[5];
    private JPanel pnlmain = new JPanel();
    private JButton btnsave;
    private JButton btnabort;
    private Box up = Box.createHorizontalBox();
    private Box midup = Box.createHorizontalBox();
    private Box mid = Box.createHorizontalBox();
    private Box midbot = Box.createHorizontalBox();
    private Box bot = Box.createHorizontalBox();
    private Box btnBox = Box.createHorizontalBox();

    public editFrame(ArrayList<String> list) {
        String[] vals = list.toArray(new String[list.size()]);


        up.setPreferredSize(new Dimension(380, 20));
        midup.setPreferredSize(new Dimension(380, 20));
        mid.setPreferredSize(new Dimension(380, 20));
        midbot.setPreferredSize(new Dimension(380, 20));
        bot.setPreferredSize(new Dimension(380, 20));
        btnBox.setPreferredSize(new Dimension(380, 20));

        btnsave = new JButton(contact.bundle.getString("detailbtnsave"));
        btnabort = new JButton(contact.bundle.getString("detailbtnabort"));
        btnabort.addActionListener(this);
        btnsave.addActionListener(this);
        lbls[0] = new JTextField(vals[0]);
        lbls[1] = new JTextField(vals[1]);
        lbls[2] = new JTextField(vals[2]);
        lbls[3] = new JTextField(vals[3]);
        lbls[4] = new JTextField(vals[4]);
        up.add(lbls[0]);
        midup.add(lbls[1]);
        mid.add(lbls[2]);
        midbot.add(lbls[3]);
        bot.add(lbls[4]);
        btnBox.add(btnsave);
        btnBox.add(btnabort);

        for(int i=0; i < vals.length; i++) {
            lbls[i].setText(vals[i]);
        }

        pnlmain.add(up);
        pnlmain.add(midup);
        pnlmain.add(mid);
        pnlmain.add(midbot);
        pnlmain.add(bot);
        pnlmain.add(btnBox);
        this.setSize(400, 300);
        this.setVisible(true);
        this.setContentPane(pnlmain);
    }

    public void actionPerformed(ActionEvent e) {
        Object source = e.getSource();
        if(source==btnsave) {
            String name = lbls[0].getText();
            String firstname = lbls[1].getText();
            String adress = lbls[2].getText();
            String bday = lbls[3].getText();
            String email = lbls[4].getText();

            try {
                save(name, firstname, adress, bday, email);
            } catch (IOException e1) {
                e1.printStackTrace();
            }
        }

        if(source==btnabort) {
            this.dispose();
        }
    }

    private void save(String name, String firstname, String adress, String bday, String email) throws IOException {
        File contactFile = new File(contact.maindir + "\\" + firstname + " " + name);
        contactFile.createNewFile();
        BufferedWriter file = new BufferedWriter(new FileWriter(contactFile));
        ArrayList<String> data = new ArrayList<String>();
        data.add(name);
        data.add(firstname);
        data.add(adress);
        data.add(bday);
        data.add(email);
        try {
            for(int i = 0; i < data.size(); i++) {
                file.write(data.get(i).toString() + "\r\n");
                this.dispose();
            }
            file.close();
        } catch(IOException e) {
            JOptionPane.showMessageDialog(null, contact.bundle.getString("saveconterror"));
            e.printStackTrace();
            throw new IOException();
        }
    }
}
