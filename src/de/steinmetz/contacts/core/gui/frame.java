package de.steinmetz.contacts.core.gui;

import de.steinmetz.contacts.core.OptionManager;
import de.steinmetz.contacts.core.contact;

import javax.swing.*;
import javax.swing.UIManager.LookAndFeelInfo;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Locale;
import java.util.ResourceBundle;
import java.util.prefs.BackingStoreException;
import java.util.prefs.Preferences;

//$java -Duser.language=en/de frame
public class frame extends JFrame implements ActionListener {
	String baseName = "resources.contacts";
	public static ResourceBundle bundle;
	String result = "";
	String currlang;
	Preferences prefs = Preferences.userRoot();
	Preferences cfg = prefs.node("Contacts");
	private JButton Button1, Button2, Button3, Button4, Button5, Button6, saveOptions;
	private JPanel Group1, Group2, Group3;
	private static JLabel saveDirlbl;
	private static JTextField saveDirtxt;
	File toRead;
	String[] dats;
	JList list;
	ArrayList<String> datsc = new ArrayList<String>();
	contact cont;
	public LookAndFeelInfo[] laf = UIManager.getInstalledLookAndFeels();
	
	
	public frame() throws IOException, BackingStoreException {
		super("YourContacts");
		doConditionChecks();
		cont = new contact();
		toRead = new File(contact.maindir.toString());
		dats = toRead.list();
		String lang = getLang();
		//Locale.setDefault( new Locale(currlang) );
		bundle = ResourceBundle.getBundle( baseName + "_" + getLang());
		createComponents();
		System.out.println();
		setFormLayout();
		try {
			for(int i = 0; i < dats.length; i++) {
				datsc.add(dats[i]);
			}
		} catch(NullPointerException e) {
			System.out.println(e);
		}
		JPanel pnlmain = new JPanel();
		pnlmain.setLayout(new GridLayout());
		JScrollPane listScroller = new JScrollPane(list);
		listScroller.setPreferredSize(new Dimension(250, 230));
		listScroller.setAlignmentY((float) Group1.getY() + 3);
		Group1.add(listScroller);
		Group3.add(Button1);
		Group3.add(Button2);
		Group3.add(Button4);
		Group3.add(Button3);
		Group2.add(saveDirlbl);
		Group2.add(saveDirtxt);
		Group2.add(saveOptions);
		Group2.add(Button5);
		Group2.add(Button6);
		pnlmain.add(Group1);
		pnlmain.add(Group2);
		pnlmain.add(Group3);
		setContentPane(pnlmain);
		this.setResizable(false);
		try
        {
          UIManager.setLookAndFeel (laf[0].getClassName());
        }
        catch (Exception x){}
		SwingUtilities.updateComponentTreeUI (this.getContentPane());
	}

	private void doConditionChecks() {
		if(OptionManager.getOption("saveDir") == null) {
			cfg.put("saveDir", "C:\\Users\\" + System.getProperty("user.name") + "\\Documents\\Contacts");
		}
		
		if(OptionManager.getOption("currentlang") == null) {
			cfg.put("currentlang", "de");
		}
		
		if(OptionManager.getOption("version") == null) {
			cfg.put("version", "2.0.1");
		}
	}

	public String getLang() throws BackingStoreException {
		doConditionChecks();
		currlang = OptionManager.getOption("currentlang");
		
		return currlang;
	}

	public void createComponents() {
		Button1 = new JButton(bundle.getString("create"));
		Button2 = new JButton(bundle.getString("delete"));
		Button3 = new JButton(bundle.getString("close"));
		Button4 = new JButton(bundle.getString("edit"));
		Button5 = new JButton("Deutsch");
		Button6 = new JButton("English");
		list = new JList(dats);
		saveOptions = new JButton(bundle.getString("btnapply"));
		saveDirlbl = new JLabel(bundle.getString("savedirlbl"));
		saveDirtxt = new JTextField(contact.maindir.toString());
		Group1 = new JPanel();
		Group2 = new JPanel();
		Group3 = new JPanel();
		Button1.addActionListener(this);
		Button2.addActionListener(this);
		Button3.addActionListener(this);
		Button4.addActionListener(this);
		Button5.addActionListener(this);
		Button6.addActionListener(this);
		saveOptions.addActionListener(this);
		cont = new contact();
	}
	
	public void setFormLayout() {
		Button1.setFont(new Font("Arial", Font.PLAIN, 20));
		Button2.setFont(new Font("Arial", Font.PLAIN, 20));
		Button3.setFont(new Font("Arial", Font.PLAIN, 20));
		Button4.setFont(new Font("Arial", Font.PLAIN, 20));
		Button1.setPreferredSize(new Dimension(200, 55));
		Button2.setPreferredSize(new Dimension(200, 55));
		Button3.setPreferredSize(new Dimension(200, 55));
		Button4.setPreferredSize(new Dimension(200, 55));
		Group1.setPreferredSize(new Dimension(200, 200));
		Group2.setPreferredSize(new Dimension(430, 70));
		Group3.setPreferredSize(new Dimension(750, 70));
		Group1.setBorder(BorderFactory.createTitledBorder(bundle.getString("brdrcontacts")));
		Group2.setBorder(BorderFactory.createTitledBorder(bundle.getString("brdrsettings")));
		Group3.setBorder(BorderFactory.createTitledBorder(bundle.getString("brdractions")));
		Group3.setAlignmentX(RIGHT_ALIGNMENT);
		Group3.setAlignmentY(CENTER_ALIGNMENT);
		Group1.setAlignmentX(LEFT_ALIGNMENT);
		Button1.setAlignmentX(CENTER_ALIGNMENT);
		Button1.setAlignmentY(BOTTOM_ALIGNMENT);
		Button2.setAlignmentX(CENTER_ALIGNMENT);
		Button2.setAlignmentY(BOTTOM_ALIGNMENT);
		Button3.setAlignmentX(CENTER_ALIGNMENT);
		Button3.setAlignmentY(BOTTOM_ALIGNMENT);
		Button4.setAlignmentX(CENTER_ALIGNMENT);
		Button4.setAlignmentY(BOTTOM_ALIGNMENT);
	}
	
	public void Update() throws IOException, BackingStoreException
	{
		this.dispose();
		new UpdateFrame();
	}
	
	@Override
	public void actionPerformed(ActionEvent arg0) {
		Object Source = arg0.getSource();
		if(Source==Button1) {
			this.dispose();
			new CreateFrame();
		}
		if(Source==Button3) {
			System.exit(0);
		}
		
		if(Source==Button2) {
			contact Contact = new contact();
			Contact.delete(list.getSelectedIndex());
			this.dispose();
			try {
				new UpdateFrame();
			} catch (IOException e) {
				e.printStackTrace();
			} catch (BackingStoreException e) {
				e.printStackTrace();
			}
		}
		
		if(Source==Button4) {
			int item = list.getSelectedIndex();
			try {
				contact.detail(item);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
		if(Source == Button5) {
			cfg.put("currentlang", "de");
			currlang = "de";
		    this.dispose();
		    try {
				new UpdateFrame();
			} catch (IOException | BackingStoreException e) {
				e.printStackTrace();
			}
		}
		
		if(Source==Button6) {
		    cfg.put("currentlang", "en");
		    currlang = "en";
		    this.dispose();
		    try {
				new UpdateFrame();
			} catch (IOException | BackingStoreException e) {
				e.printStackTrace();
			}
		}
		
		if(Source==saveOptions) {
			OptionManager.saveOption("saveDir", saveDirtxt.getText().toString());
			JOptionPane.showMessageDialog(null, bundle.getString("restart"));
		}
		
	}

}