package de.steinmetz.contacts.core.gui;

import de.steinmetz.contacts.utils.Frame;
import de.steinmetz.contacts.core.contact;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.prefs.BackingStoreException;

public class CreateFrame extends Frame implements ActionListener {
	
	private JTextField[] lbls = new JTextField[5];
	private JPanel pnl = new JPanel();
	private JButton btnsave; 
	private JButton btnabort;
	private Box up = Box.createHorizontalBox();
	private Box midup = Box.createHorizontalBox();
	private Box mid = Box.createHorizontalBox();
	private Box midbot = Box.createHorizontalBox();
	private Box bot = Box.createHorizontalBox();
	private Box btnBox = Box.createHorizontalBox();
	
	public CreateFrame() {
		super(400, 300, "");
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		up.setPreferredSize(new Dimension(380, 20));
		midup.setPreferredSize(new Dimension(380, 20));
		mid.setPreferredSize(new Dimension(380, 20));
		midbot.setPreferredSize(new Dimension(380, 20));
		bot.setPreferredSize(new Dimension(380, 20));
		btnBox.setPreferredSize(new Dimension(380, 20));
		
		btnsave = new JButton(contact.bundle.getString("createbtncreate"));
		btnabort = new JButton(contact.bundle.getString("detailbtnabort"));
		btnabort.addActionListener(this);
		btnsave.addActionListener(this);
		lbls[0] = new JTextField(contact.bundle.getString("prename"));
		lbls[1] = new JTextField(contact.bundle.getString("name"));
		lbls[2] = new JTextField(contact.bundle.getString("address"));
		lbls[3] = new JTextField(contact.bundle.getString("bday"));
		lbls[4] = new JTextField(contact.bundle.getString("email"));
		up.add(lbls[0]);
		midup.add(lbls[1]);
		mid.add(lbls[2]);
		midbot.add(lbls[3]);
		bot.add(lbls[4]);
		btnBox.add(btnsave);
		btnBox.add(btnabort);
		
		pnl.add(up);
		pnl.add(midup);
		pnl.add(mid);
		pnl.add(midbot);
		pnl.add(bot);
		pnl.add(btnBox);
		this.setSize(400, 300);
		this.setVisible(true);
		this.setContentPane(pnl);
		
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		Object source = e.getSource();
		if(source == btnsave) {
			String prename = lbls[0].getText();
			String name = lbls[1].getText();
			String address = lbls[2].getText();
			String bday = lbls[3].getText();
			String email = lbls[4].getText();
			try {
				contact.add(name, prename, address, bday, email);
				this.dispose();
			} catch (IOException e1) {
				e1.printStackTrace();
			}
		}
		
		if(source == btnabort) {
			this.dispose();
			try {
				new UpdateFrame();
			} catch (IOException e1) {
				e1.printStackTrace();
			} catch (BackingStoreException e1) {
				e1.printStackTrace();
			}
		}
		
	}
}
