package de.steinmetz.contacts.core;

import java.io.IOException;

public interface IContact {
	public void delete(String Vorname, String Name);
	public void add(String Name,
		   			String Vorname,
		   			String Adresse,
		   			String Geburtsdatum,
		   			String EMail
		   			) throws IOException;
	void detail(int item) throws IOException;
}
