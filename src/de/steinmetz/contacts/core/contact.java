package de.steinmetz.contacts.core;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.ResourceBundle;
import java.util.prefs.BackingStoreException;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

import de.steinmetz.contacts.core.OptionManager;
import de.steinmetz.contacts.core.gui.UpdateFrame;
import de.steinmetz.contacts.core.gui.editFrame;

public class contact extends JFrame {

	static String baseName = "resources.contacts";
	public static ResourceBundle bundle = ResourceBundle.getBundle( baseName );
	
	public static String[] contactarray;
	public static File maindir = new File(OptionManager.getOption("saveDir"));
	public static ArrayList<String> values = new ArrayList<String>();
	private File demoContact = new File(maindir + "Demo Demo");
	
	public contact() {
		super("YourContacts");
		maindir.mkdir();
		if(maindir.length() > 0)
			try {
				demoContact.createNewFile();
			} catch (IOException e) {
				JOptionPane.showMessageDialog(null, "Can't create new file at " + maindir.toString());
			}

	}

	public static void detail(int item) throws IOException 
	{
		try {
			String[] files = maindir.list();
			String toShow = files[item];
			BufferedReader read = new BufferedReader(new FileReader(maindir + "\\" + toShow));
			String line = read.readLine();
			
			while(line != null) 
			{
				values.add(line);
				line = read.readLine();
			}
			read.close();
			new editFrame(values);
			values.clear();
			read.reset();
			read.close();
			line = "";
		} catch(IOException e) {
			e.printStackTrace();
		}
	}

	public void delete(int item) {
		String[] files = maindir.list();
		String todel = files[item];
		File f = new File(maindir + "\\" + todel);
		f.delete();
	}

	public static void add(String name, String firstname, String adress,
					String bday, String email) throws IOException {

		File contactFile = new File(maindir + "\\" + firstname + " " + name);
		contactFile.createNewFile();
		BufferedWriter file = new BufferedWriter(new FileWriter(contactFile));
		ArrayList<String> data = new ArrayList<String>();
		data.add(name);
		data.add(firstname);
		data.add(adress);
		data.add(bday);
		data.add(email);
		try {
				for(int i = 0; i < data.size(); i++) {
					file.write(data.get(i).toString() + "\r\n");
			}
			file.close();
			new UpdateFrame();
		} catch(IOException | BackingStoreException e) {
			JOptionPane.showMessageDialog(null, bundle.getString("createconterror"));
			e.printStackTrace();
			throw new IOException();
		}
		
	}
}