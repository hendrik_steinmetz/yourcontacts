package de.steinmetz.contacts.core;

import java.util.prefs.Preferences;

public class OptionManager {

	private static Preferences prefs = Preferences.userRoot();
	private static Preferences cfg = prefs.node("Contacts");
	
	public static void saveOption(String node, String value) {
		cfg.put(node, value);
	}
	
	
	public static String getOption(String node) {
		return cfg.get(node, null);
	}
	
}