package de.steinmetz.contacts.updater;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.prefs.Preferences;

import javax.swing.JOptionPane;

import de.steinmetz.contacts.core.OptionManager;

public class Updater {

	URL host;
	String version;
	Preferences prefs = Preferences.userRoot();
	Preferences cfg = prefs.node("Contacts");
	OptionManager o = new OptionManager();
	
	
	public Updater() throws IOException {
		
		String localversion = o.getOption("version");
		
		try {
			host = new URL("https://raw.githubusercontent.com/hsteinmetz/YourContacts/master/build/version.txt");
			BufferedReader br = new BufferedReader(new InputStreamReader(host.openStream()));
			String version = br.readLine();
			
			if(!localversion.toString().equals(version)) {
				JOptionPane.showMessageDialog(null, "Update found. Go to https://github.com/hsteinmetz/YourContacts/build\nand look for the latest version.");
				
			} else {
				JOptionPane.showMessageDialog(null, "No update found. Booting.");
			}
			
		} catch (Exception d) {
			d.printStackTrace();
		}
	}
	
}
