/*
 * @author Hendrik Steinmetz
 */
package de.steinmetz.contacts;

import java.io.IOException;
import java.util.prefs.BackingStoreException;

import javax.swing.JFrame;

import de.steinmetz.contacts.core.gui.frame;
import de.steinmetz.contacts.updater.Updater;

public class Main {
	
	public Main() throws IOException, BackingStoreException {
		frame f = new frame();
		f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		f.setSize(800, 300);
		f.setVisible(true);
	}
	
	
	
	public static void main(String[] args) throws IOException, BackingStoreException {
		new Updater();
		new Main();
	}
	
}