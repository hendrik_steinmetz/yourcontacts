# YourContacts
YourContacts is an easy-to-use contact-management-software, developed by Hendrik Steinmetz.
You can use it for commercial and/or private use for free.
Please report any bugs or issues at https://github.com/hsteinmetz/YourContacts/issues/new

For more information, head to https://hsteinmetz.github.io/YourContacts
